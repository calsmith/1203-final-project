class Course:
    def __init__(self, grade, subject, credits, major: bool = False):
        self.grade = grade
        self.subject = subject
        self.credits = credits
        self.major = major



class GPA:
    
    GRADES = {
        'A+': 4.0,
        'A': 4.0,
        'A-': 3.7,
        'B+': 3.3,
        'B': 3.0,
        'B-': 2.7,
        'C+': 2.3,
        'C': 2.0,
        'C-': 1.7,
        'D+': 1.3,
        'D': 1.0,
        'F': 0,
    }

    def __init__(self):
        self.courses = []
        self.major = []
        self.credits = 0

    def addCourse(self, course):
        self.courses.append(course)
        if course.major == True:
            self.major.append(course)

    def calculateGPA(self):
        total = 0.0
        credits_attempted = 0.0
        for course in self.courses:
            total += self.GRADES[course.grade] * course.credits
            credits_attempted += course.credits

        return total/credits_attempted
    
    def what_if(self):
        original_grades = [course.grade for course in self.courses]  # to return the grades back to normal

        courses = [course.subject for course in self.courses]  # just to print the available subjects for the user to pick from
        for course in courses:
            print(f"{course}\t")

        # loop to prompt the user to pick a course to change
        # also allows the user to exit the loop when they're done changing grades
        add_more_classes = "Y"
        while add_more_classes == "Y":
            test_subject = input("Select a class from the list above that you would like to test: ").lower()
            while test_subject not in [course.lower() for course in courses] and test_subject != "N":
                test_subject = input("Invalid input, please enter a course from the list above. To exit, type 'N': ")

            # find the course corresponding to the user input and update the grade, storing the original grade in original_grades_hold
            for course in self.courses:
                if course.sub == test_subject:
                    # continues to prompt the user until they enter a valid grade
                    updated_grade = input("Enter a grade you would like to test: ")
                    while updated_grade not in self.GRADES:
                        updated_grade = input(f"Invalid entry. Enter from the following: {list(self.GRADES.keys())}: ")
                    course.grade = updated_grade  # updates the grade

            # loop that gives the user the option to change grades in other classes
            add_more_classes = input("Would you like to change the grade in other classes? ").upper()
            while add_more_classes != "N" and add_more_classes != "Y":
                add_more_classes = input("Invalid entry. Type 'Y' if you would like to change grades in other classes. Type 'N' if you are done: ")

        print("Updated GPA:")
        print(self.calculateGPA())
        for course_index in range(len(self.courses)):
            self.courses[course_index].grade = original_grades[course_index]  # each grade is overwritten back to the original grade

    def MajorGPA(self):
        total = 0.0
        credits_attempted = 0.0
        for course in self.major:
            total += self.GRADES[course.grade] * course.credits
            credits_attempted += course.credits

        return total/credits_attempted
    
    # Read course information from file and create GPA object
    def read_as_file(self, file):
        
        with open(file, "r") as a_file:
            for line in a_file:
                line = line.strip().split()
                self.courses.append(Course(line[1], line[0], int(line[2])))
        
# read as file
courses = GPA()
courses.read_as_file("test_file.txt")

# major GPA
print("Before 'major' variable is changed:")
for course_index in range(len(courses.courses)):
    if courses.courses[course_index].major == True:
        print(courses.courses[course_index].subject)
print()

courses.courses[2].major = True
courses.major.append(courses.courses[2])

print("After 'major' variable is changed:")
for course_index in range(len(courses.courses)):
    if courses.courses[course_index].major == True:
        print(courses.courses[course_index].subject)
print()

# calculate GPA
print("Calculate GPA:")
print(courses.calculateGPA())
print()

# major GPA
print("Major GPA:")
print(courses.MajorGPA())
print()

# what-if
courses.what_if()