from gpa import Course
from gpa import GPA
import pytest


def test_majorgpa():


    gpa = GPA()
    gpa.read_as_file("test_file.txt")

    gpa.courses[1].major = True
    gpa.major.append(gpa.courses[1])
    assert gpa.courses[1].major == True

def test_length():
    gpa = GPA()
    gpa.read_as_file("test_file.txt")
    gpa.courses[1].major = True
    gpa.major.append(gpa.courses[1])
    assert len(gpa.major) == 1

def test_major():
    gpa = GPA()
    gpa.read_as_file("test_file.txt")

    gpa.courses[1].major = True
    gpa.major.append(gpa.courses[1])
    gpa.courses[1].grade == "A"
    assert gpa.MajorGPA() == 3.0

def test_courses():
    gpa = GPA()
    gpa.read_as_file("test_file.txt")
    gpa.courses[1].major = True
    gpa.major.append(gpa.courses[1])
    assert len(gpa.courses) == 4
    
    
