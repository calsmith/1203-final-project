import pytest
from gpa import GPA

gpa = GPA()
gpa.read_as_file("test_file.txt")
    
def test_subject():
    assert gpa.courses[0].subject == "Chem"
    assert gpa.courses[0].subject != "Bio"

def test_grades():
    assert gpa.courses[0].grade == "A"
    assert gpa.courses[0].grade != "B"

def test_credits():
    assert gpa.courses[0].credits == 4
    assert gpa.courses[0].credits != 3
 
def test_len():
    assert len(gpa.courses) == 4
    assert len(gpa.courses) != 3