import pytest
from gpa import GPA
from gpa import Course

test_gpa = GPA()

def test_gpa_calc():
    test_1 = Course("A", "Bio", 4)
    test_2 = Course("B-", "Calc", 3)
    test_3 = Course("C", "Psyche", 2)
    test_gpa.addCourse(test_1)
    assert (test_gpa.calculateGPA()) == 4.00
    test_gpa.addCourse(test_2)
    assert test_gpa.calculateGPA() > 3.43
    test_gpa.addCourse(test_3)
    assert test_gpa.calculateGPA() > 3.11
